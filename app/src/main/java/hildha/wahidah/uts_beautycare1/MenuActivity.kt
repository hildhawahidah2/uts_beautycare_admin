package hildha.wahidah.uts_beautycare1

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity(), View.OnClickListener {

    val PRODUCTS: Int = 100
    val TIPS: Int = 101
    var fbaut = FirebaseAuth.getInstance()


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button -> {
                var intent = Intent(this, ProductsActivity::class.java)
                startActivityForResult(intent, PRODUCTS)
            }
            R.id.button2 -> {
                var intent = Intent(this, TipsActivity::class.java)
                startActivityForResult(intent, TIPS)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        button.setOnClickListener(this)
        button2.setOnClickListener(this)
        logout.setOnClickListener {
            fbaut.signOut()
            finish()
        }
    }
}