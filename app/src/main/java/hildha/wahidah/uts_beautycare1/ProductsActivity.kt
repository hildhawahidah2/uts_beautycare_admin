package hildha.wahidah.uts_beautycare1

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_products.*

class ProductsActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "beautycare"
    val F_ID = "id"
    val F_NAME = "nama"
    val F_ADDRESS = "fungsi"
    val F_PHONE = "harga"
    var docId = ""
    lateinit var db: FirebaseFirestore
    lateinit var alStudent: ArrayList<HashMap<String, Any>>
    lateinit var adapter: SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        alStudent = ArrayList()
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if (e != null) e.message?.let { Log.d("firestore", it) }
            showData()

        }
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnInsert -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, edId.text.toString())
                hm.set(F_NAME, edNama.text.toString())
                hm.set(F_ADDRESS, edFungsi.text.toString())
                hm.set(F_PHONE, edHarga.text.toString())
                db.collection(COLLECTION).document(edId.text.toString()).set(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Successfully Added", Toast.LENGTH_SHORT)
                            .show()
                    }.addOnFailureListener { e ->
                        Toast.makeText(
                            this,
                            "Data Unsuccessfully Added :${e.message}",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                edId.setText("")
                edNama.setText("")
                edFungsi.setText("")
                edHarga.setText("")
            }
            R.id.btnUpdate -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, docId)
                hm.set(F_NAME, edNama.text.toString())
                hm.set(F_ADDRESS, edFungsi.text.toString())
                hm.set(F_PHONE, edHarga.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this, "Data Successfully Updated", Toast.LENGTH_SHORT)
                            .show()
                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(
                            this,
                            "Data Unsuccessfully Updated :${e.message}",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                edId.setText("")
                edNama.setText("")
                edFungsi.setText("")
                edHarga.setText("")
            }
            R.id.btnDelete -> {
                db.collection(COLLECTION).whereEqualTo(F_ID, docId).get()
                    .addOnSuccessListener { result ->
                        for (doc in result) {
                            db.collection(COLLECTION).document(doc.id).delete()
                                .addOnSuccessListener {
                                    Toast.makeText(
                                        this, "Data Successfully Deleted",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }.addOnFailureListener { e ->
                                    Toast.makeText(
                                        this, "Data Unsuccessfully Deleted ${e.message}",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                        }
                    }.addOnFailureListener { e ->
                        Toast.makeText(
                            this, "Data Unsuccessfully Deleted ${e.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                edId.setText("")
                edNama.setText("")
                edFungsi.setText("")
                edHarga.setText("")
            }
        }
    }


    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alStudent.get(position)
        docId = hm.get(F_ID).toString()
        edId.setText(docId)
        edNama.setText(hm.get(F_NAME).toString())
        edFungsi.setText(hm.get(F_ADDRESS).toString())
        edHarga.setText(hm.get(F_PHONE).toString())
    }


    fun showData() {
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alStudent.clear()
            for (doc in result) {
                val hm = HashMap<String, Any>()
                hm.set(F_ID, doc.get(F_ID).toString())
                hm.set(F_NAME, doc.get(F_NAME).toString())
                hm.set(F_ADDRESS, doc.get(F_ADDRESS).toString())
                hm.set(F_PHONE, doc.get(F_PHONE).toString())
                alStudent.add(hm)
            }
            adapter = SimpleAdapter(
                this, alStudent, R.layout.row_data_products,
                arrayOf(F_ID, F_NAME, F_ADDRESS, F_PHONE),
                intArrayOf(R.id.txId, R.id.txNama, R.id.txFungsi, R.id.txHarga)
            )
            lsData.adapter = adapter
        }
    }
}