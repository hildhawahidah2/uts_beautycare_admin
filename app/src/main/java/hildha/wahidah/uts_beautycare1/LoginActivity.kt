package hildha.wahidah.uts_beautycare1

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnLogin.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnLogin -> {
                var email = edEmail.text.toString()
                var password = edPassword.text.toString()
                if (email.isEmpty() || password.isEmpty()) {
                    Toast.makeText(
                        this, "Email or Password Can't be Empty",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.isIndeterminate = true
                    progressDialog.setMessage("Authenticating...")
                    progressDialog.show()
                    FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener {
                            if (!it.isSuccessful) return@addOnCompleteListener
                            progressDialog.hide()
                            Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show()
                            val intent = Intent(this, MenuActivity::class.java)
                            startActivity(intent)
                            edEmail.setText("")
                            edPassword.setText("")

                        }
                        .addOnFailureListener {
                            progressDialog.hide()
                            Toast.makeText(
                                this, "Incorrect Email or Password",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
            }
        }
    }
}
